CREATE TABLE IF NOT EXISTS my_rules
(
    id        SERIAL PRIMARY KEY,
    name      VARCHAR(255) NOT NULL,
    condition TEXT         NOT NULL,
    action    TEXT         NOT NULL
);
