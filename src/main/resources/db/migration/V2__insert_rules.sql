INSERT INTO my_rules (name, condition, action)
VALUES ('Prix supérieur à 600', 'product.price > 600', 'product.price = product.price * 0.9'),
       ('Prix inférieur à 200', 'product.price < 200', 'product.price = product.price * 0.95');
