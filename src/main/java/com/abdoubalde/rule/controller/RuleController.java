package com.abdoubalde.rule.controller;

import com.abdoubalde.product.Product;
import com.abdoubalde.rule.service.RuleService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/rules")
public class RuleController {

    private final RuleService ruleService;

    public RuleController(RuleService ruleService) {
        this.ruleService = ruleService;
    }

    @PostMapping("/apply")
    public Product applyRulesToProduct(@RequestBody Product product) {
        ruleService.applyRulesToProduct(product);
        return product;
    }
}
