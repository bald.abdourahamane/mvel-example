package com.abdoubalde.rule.repository;

import com.abdoubalde.rule.Rule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RuleRepository extends JpaRepository<Rule, Long> {
}
