package com.abdoubalde.rule.service;

import com.abdoubalde.product.Product;
import com.abdoubalde.rule.Rule;
import com.abdoubalde.rule.repository.RuleRepository;
import org.mvel2.MVEL;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RuleService {

    private final RuleRepository ruleRepository;

    public RuleService(RuleRepository ruleRepository) {
        this.ruleRepository = ruleRepository;
    }

    public void applyRulesToProduct(Product product) {
        List<Rule> rules = ruleRepository.findAll();

        Map<String, Object> mvelVariables = new HashMap<>();
        mvelVariables.put("product", product);

        for (Rule rule : rules) {
            boolean conditionResult = (boolean) MVEL.eval(rule.getCondition(), mvelVariables);
            if (conditionResult) {
                MVEL.eval(rule.getAction(), mvelVariables);
            }
        }
    }
}
